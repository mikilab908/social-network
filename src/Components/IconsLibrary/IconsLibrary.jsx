import react from 'react';

// imports
import { library } from '@fortawesome/fontawesome-svg-core';
import { faUser, faHandRock, faShare } from '@fortawesome/free-solid-svg-icons';

// export
library.add(faUser, faHandRock, faShare);
