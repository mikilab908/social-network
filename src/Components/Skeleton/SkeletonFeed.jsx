import React from 'react';

// npm imports
import styled from 'styled-components';

// consts imports

// context imports

// component imports
import SkeletonPost from './SkeletonPost';

// styled-components
const SkeletonFeedWrapper = styled.div`
  width: 100%;
  padding-bottom: 10px;
`;

// component
const SkeletonFeed = (props) => {
  return (
    <SkeletonFeedWrapper>
      {Array(3)
        .fill()
        .map((post, index) => (
          <SkeletonPost key={index} />
        ))}
    </SkeletonFeedWrapper>
  );
};

export default SkeletonFeed;
