import React from 'react';

// import
import styled from 'styled-components';

// local import
import Wrapper from '../Wrapper/Wrapper';

// component import
import NewPost from '../NewPost/NewPost';
import Post from '../../Components/Post/Post';
import SkeletonFeed from '../Skeleton/SkeletonFeed';

// styled-component
const LayoutTestWrapper = styled(Wrapper)`
  width: 100%;
`;

// component
function LayoutTest(props) {
  return (
    <LayoutTestWrapper>
      <NewPost />
      <Post userName='Filip' textContent='Zdravo, lugje, jas sum Filip...' />
      <SkeletonFeed />
    </LayoutTestWrapper>
  );
}

export default LayoutTest;
