import react, { useState, useContext, useEffect } from 'react';

// npm import
import styled from 'styled-components';
import { Link, useLocation } from '@reach/router';

// const imports
import { API } from '../../Consts/Api';

// component imports
import { Heading14, Heading16 } from '../Text/Text';

// global context
import { GlobalContext } from '../../Context/GlobalContext';

// styled component
const NavbarWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 8px 15px;
  border: 1px solid ${(props) => props.theme.text_gray};
  background-color: ${(props) => props.theme.dark_gray2};

  .logo {
    color: ${(props) => props.theme.yellow};
  }

  .mobile-menu {
    .navigation-links {
      width: 100%;
      height: 100vh;
      position: fixed;
      overflow: hidden;
      display: flex;
      flex-direction: column;
      align-items: flex-end;
      top: 0;
      right: ${(props) => (props.mobileMenuActive ? '0%' : '-110%')};
      background-color: ${(props) => props.theme.dark_gray};
      transition: right 0.3s;
    }

    h5 {
      paddind: ${(props) => (props.mobileMenuActive ? '8px 15px' : '15px 0')};
    }
  }

  .burger-menu {
    display: flex;
    flex-direction: column;

    span {
      width: 26px;
      border: 1px solid white;
      margin: 3px 0;
      border-radius: 2px;
    }
  }

  .burger-menu-x {
    width: 26px;
    height: 26px;
    margin: 8px 15px 0 0;
    display: flex;
    position: relative;

    span {
      width: 26px;
      border: 1px solid white;
      border-radius: 2px;
      position: absolute;
      top: 13px;
      left: 0px;
    }

    span:first-child {
      transform: rotate(45deg);
    }

    span: last-child {
      transform: rotate(315deg);
    }
  }
`;

// component

const Navbar = (props) => {
  const { user } = useContext(GlobalContext);
  const [mobileMenuActive, setMobileMenuActive] = useState(false);
  const [isNavbarVisible, setIsNavbarVisible] = useState(false);
  const location = useLocation();

  useEffect(() => {
    props.except &&
      setIsNavbarVisible(
        !props.except.some((exceptRoute) => {
          return location.pathname.indexOf(exceptRoute) > -1;
        })
      );
  }, [location.pathname]);

  return (
    isNavbarVisible && (
      <NavbarWrapper mobileMenuActive={mobileMenuActive} className='navbar'>
        <Link className='navbar-logo' to={API.paths.homePage}>
          <Heading14 className='logo'>The Network</Heading14>
        </Link>

        <div className='mobile-menu'>
          <ul className='navigation-links'>
            <div
              className='burger-menu-x'
              onClick={() => setMobileMenuActive(false)}
            >
              <span></span>
              <span></span>
            </div>

            <li>
              <Link
                onClick={() => setMobileMenuActive(false)}
                to={API.paths.myProfile.replace(
                  '{USER_NAME}',
                  user.userName.replace(' ', '').toLowerCase()
                )}
              >
                <Heading16>My Profile</Heading16>
              </Link>
            </li>
          </ul>
        </div>

        <div className='burger-menu' onClick={() => setMobileMenuActive(true)}>
          <span></span>
          <span></span>
          <span></span>
        </div>
      </NavbarWrapper>
    )
  );
};

export default Navbar;
