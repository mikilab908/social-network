import React, { useContext } from 'react';

// npm imports
import styled from 'styled-components';

// consts imports

// context imports
import { GlobalContext } from '../../Context/GlobalContext';

// component imports
import Button from '../../Components/Button/Button';
import { Heading16 } from '../../Components/Text/Text';
import { navigate } from '@reach/router';
import { API } from '../../Consts/Api';

// styled-components
const LoginWrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: ${(props) => props.theme.dark_gray};

  input {
    border: 1px solid ${(props) => props.theme.text_gray};
    border-radius: 5px;
    padding: 8px 0;
    margin-top: 15px;
    margin-bottom: 15px;
  }

  input,
  button {
    width: 80%;
  }
`;
// component

const Login = () => {
  const { user, setUser } = useContext(GlobalContext);

  const login = () => {
    localStorage.setItem('user', JSON.stringify(user));
    navigate(API.paths.newsFeed);
  };

  return (
    <LoginWrapper>
      <Heading16>Choose your username</Heading16>
      <input
        type='text'
        onChange={(e) => setUser({ ...user, userName: e.target.value })}
      />
      <Button
        type='button'
        text='Login'
        disabled={user.userName === ''}
        onClick={login}
      />
    </LoginWrapper>
  );
};

export default Login;
