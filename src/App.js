import React from 'react';

import './Components/IconsLibrary/IconsLibrary';

// npm imports
import { ThemeProvider } from 'styled-components';
import { Router, LocationProvider } from '@reach/router';

// consts imports
import GlobalStyle from './Consts/GlobalStyle';
import theme from './Consts/Theme';
import { API } from './Consts/Api';


// global context
import { GlobalContextProvider } from './Context/GlobalContext';

// components imports
import LayoutTest from './Components/LayoutTest/LayoutTest';
import MyProfile from './Components/MyProfile/MyProfile';
import Navbar from './Components/Navbar/Navbar';
import Login from './Pages/Login/Login';
import NewsFeed from './Pages/NewsFeed/NewsFeed';

const App = () => (
  <GlobalContextProvider>
    <LocationProvider>
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <Navbar except={['/login']} />
        <Router>
          <Login path={API.paths.login} />
          <LayoutTest path={API.paths.layout} />
          <MyProfile
            path={(API.paths.myProfile.replace('{USER_NAME'), ':userName')}
          />
          <NewsFeed path={API.paths.newsFeed} />
        </Router>
      </ThemeProvider>
    </LocationProvider>
  </GlobalContextProvider>
);

export default App;
